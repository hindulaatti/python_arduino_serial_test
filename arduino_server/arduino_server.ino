#include <Servo.h>
#define INPUT_SIZE 50
#define ARRAY_LENGTH(x) (sizeof(x)/sizeof(*x))
#define FLOAT_TO_INT(x) ((x)>=0?(int)((x)+0.5):(int)((x)-0.5))
//uncomment this line if using a Common Anode LED
//#define COMMON_ANODE

// RGB stuff
int redPin = 6;
int bluePin = 5;
int greenPin = 3;
int rgb[] = {0,0,0,0};

// Servu stuff
int servoPin = 9;
Servo myservo;
int pos = 0;    // variable to store the servo position
int servoAngle = 0;

// Alarm stuff
int PIRPin = 7;
int alarmOn = 0;
int alarmTriggered = 0;

// Serial stuff
char reading[INPUT_SIZE + 1];
char* token = NULL;
char allTokens[10][14];

 
void setup()
{
  Serial.begin(115200);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  pinMode(PIRPin, INPUT);
  myservo.attach(servoPin);
  setColor(rgb[0], rgb[1], rgb[2]);
  Serial.println("Ready to serve");
}

void loop()
{
  if(digitalRead(PIRPin) == HIGH && alarmOn == 1)
  {
    alarmTriggered = 1;
    alarm();
  }
  strcpy(reading, "");
  // if serial data available, process it
  while (Serial.available () > 0)
    processIncomingByte (Serial.read ());
  
  delay(2);
  if ( !strcmp(reading, "") == 0 )
  {
    token = strtok(reading, ","); // Split the string
    int index = 0;
    // Go through all the splitted things
    while(token != NULL && index < 10)
    {
      strcpy(allTokens[index], token);
      token = strtok(NULL, ",");
      index++;
    }
  
    if (strcmp(allTokens[0], "rgb") == 0) // strcmp() returns 0 if the strings are equal
    {
      rgb[0] = atoi(allTokens[1]);
      rgb[1] = atoi(allTokens[2]);
      rgb[2] = atoi(allTokens[3]);
      setColor(rgb[0], rgb[1], rgb[2]);
      Serial.print("RGB ");
      Serial.print(rgb[0]);
      Serial.print(", ");
      Serial.print(rgb[1]);
      Serial.print(", ");
      Serial.println(rgb[2]);
    }
    else if (strcmp(allTokens[0], "servo") == 0) // strcmp() returns 0 if the strings are equal
    {
      servoAngle = atoi(allTokens[1]);
      setServo(servoAngle);
      Serial.print("Servo ");
      Serial.println(servoAngle);
    }
    else if (strcmp(allTokens[0], "alarm") == 0) // strcmp() returns 0 if the strings are equal
    {
      alarmOn = atoi(allTokens[1]);
      Serial.print("Alarm ");
      Serial.println(alarmOn);
    }
    else if (strcmp(allTokens[0], "alarmTriggered") == 0) // strcmp() returns 0 if the strings are equal
    {
      alarmTriggered = 1;
      alarm();
    }
    else
    {
      Serial.print("No command '");
      Serial.print(allTokens[0]);
      Serial.println("'");
    }
    Serial.print("PIR state ");
    Serial.println(digitalRead(PIRPin));
  }
}

// here to process incoming serial data after a terminator received
void process_data (const char * data)
{
  strcpy(reading, data);
}
 
void processIncomingByte (const byte inByte)
{
  static char input_line [INPUT_SIZE];
  static unsigned int input_pos = 0;
  switch (inByte)
  {
    case 10:   // end of text
      input_line [input_pos] = 0;  // terminating null byte
  
      // terminator reached! process input_line here ...
      process_data (input_line);
  
      // reset buffer for next time
      input_pos = 0;
      break;

    case 13:   // discard carriage return
      break;

    default:
      // keep adding if not full ... allow for terminating null byte
      if (input_pos < (INPUT_SIZE - 1))
      {
          input_line [input_pos] = inByte;
          input_pos++;
      }
      break;
  }
}

void setColor(int red, int green, int blue)
{
  #ifdef COMMON_ANODE
    red = 255 - red;
    green = 255 - green;
    blue = 255 - blue;
  #endif
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);
}

void setServo (int angle)
{
  if(servoAngle > 180 || servoAngle < 0)
  {
    Serial.print("Servo position wrong");
  }
  myservo.write(servoAngle);
}

void alarm()
{
  int angle = 180;
  while(alarmTriggered){
    Serial.println("alarm!!!!");
    angle = abs(angle - 180);
    myservo.write( angle );
    setColor(255,0,0);
    delay(500);
    setColor(0,0,0);
    angle = abs(angle - 180);
    myservo.write( angle );
    delay(300);
    while (Serial.available () > 0)
      processIncomingByte (Serial.read ());
      
    delay(200);
    if ( !strcmp(reading, "") == 0 )
    {
      token = strtok(reading, ","); // Split the string
      int index = 0;
      // Go through all the splitted things
      while(token != NULL && index < 10)
      {
        strcpy(allTokens[index], token);
        token = strtok(NULL, ",");
        index++;
      }
      if (strcmp(allTokens[0], "alarm") == 0) // strcmp() returns 0 if the strings are equal
      {
        alarmTriggered = 0;
        alarmOn = atoi(allTokens[1]);
        Serial.print("Alarm ");
        Serial.println(alarmOn);
      }
    }
  }
  setColor(rgb[0], rgb[1], rgb[2]);
  myservo.write(servoAngle);
}
