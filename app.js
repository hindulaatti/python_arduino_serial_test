const express = require('express')
const app = express()
const Port = 3000
const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const port = new SerialPort('COM6', {
	baudRate: 115200
})

app.use(express.urlencoded({extended: true}));

const parser = port.pipe(new Readline({ delimiter: '\r\n' }))
parser.on('data', console.log)

const writeLine = (string) => {
	port.write(string + "\n", function(err) {
		if (err) {
		return console.log('Error on write: ', err.message)
		}
		console.log('message written')
	})
}

const writeRGB = (r, g, b) => {
	message = "rgb," + r + "," + g + "," + b
	writeLine(message)
}

const writeServo = (angle) => {
	message = "servo," + angle
	writeLine(message)
}

const form = `
<form action='/rgb' content-type='application/json'>
Red:<br>
<input type='text' name='red' value=''><br>
Greem:<br>
<input type='text' name='green' value=''><br>
BLue:<br>
<input type='text' name='blue' value=''><br><br>
<input type='submit' value='Submit'>
</form>

<form action='/servo' content-type='application/json'>
Servo:<br>
<input type='text' name='servo' value=''><br><br>
<input type='submit' value='Submit'>
</form>

<form action='/alarm' content-type='application/json'>
Alarm:
<input type='submit' value='Submit'>
</form>

<form action='/alarmTrigger' content-type='application/json'>
Trigger alarm:
<input type='submit' value='Submit'>
</form>

<form action='/alarmOff' content-type='application/json'>
Stop alarm:
<input type='submit' value='Submit'>
</form>
`

app.get('/', (req, res) => {
	res.send(form)
})

app.get('/rgb', (req, res) => {
	writeRGB(req.query.red, req.query.green, req.query.blue)
	res.send(form)
})

app.get('/servo', (req, res) => {
	writeServo(req.query.servo)
	res.send(form)
})

app.get('/alarm', (req, res) => {
	writeLine("alarm")
	res.send(form)
})

app.get('/alarmTrigger', (req, res) => {
	writeLine("alarmTriggered")
	res.send(form)
})

app.get('/alarmOff', (req, res) => {
	writeLine("alarmoff")
	res.send(form)
})

app.listen(Port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${Port}`)
})