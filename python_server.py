import serial
import time
ser = serial.Serial('COM6', 9600, timeout=5)
time.sleep(2) # wait for Arduino

# Reads stuff from com port before newline
class ReadLine:
	def __init__(self, s):
		self.buf = bytearray()
		self.s = s

	def readline(self):
		i = self.buf.find(b"\n")
		if i >= 0:
			r = self.buf[:i+1]
			self.buf = self.buf[i+1:]
			return r
		while True:
			i = max(1, min(2048, self.s.in_waiting))
			data = self.s.read(i)
			i = data.find(b"\n")
			if i >= 0:
				r = self.buf + data[:i+1]
				self.buf[0:] = data[i+1:]
				return r
			else:
				self.buf.extend(data)


def readln():
	s = ReadLine(ser)
	try:
		msg = s.readline()

		return msg
	except:
		return 0;
		
def sendRGB(r, g ,b):
	message = "asd," + str(r) + "," + str(g) + "," + str(b)
	ser.write(message.encode('utf-8'))
	ser.write(b'\n')

while(True):
	sendRGB(200, 100, 50);
	message = readln()
	print(message)
	time.sleep(1)